<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array();

$autoload['helper'] = array("url","url_helper");

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();