<?php

if( !defined('BASEPATH') )
    exit('No direct script access allowed');

/*
  Extended the core Router class to allow for sub-sub-folders in the controllers directory.
 */

function debug($var, $ndie = 0){ echo"<pre>";print_r($var);echo"</pre>";if(!$ndie)exit; }

class MY_Router extends CI_Router{

    function __construct(){
        parent::__construct();
    }

    function _validate_request( $segments )
    {
        if( count($segments) == 0 ){
            return $segments;
        }

        # Verifica se o controller existe na raiz da pasta de controllers
        if( file_exists(APPPATH.'controllers/'.$segments[0].'.php') ){
            return $segments;
        }
        
        # Se a controller está em uma sub-pasta
        if( is_dir(APPPATH.'controllers/'.$segments[0]) ){
            # Define o diretório e remove ele do array de segmentos
            $this->set_directory($segments[0]);
            $segments = array_slice($segments, 1);
            
            # Enquanto o array de segmentos existir e seus elementos forem diretórios
            while( count($segments) > 0 && is_dir(APPPATH.'controllers/'.$this->directory.$segments[0]) ){
                # Define o diretório e remove ele do array de segmentos
                $this->set_directory($this->directory.$segments[0]);
                $segments = array_slice($segments, 1);
            }

            # Se ainda existirem segmetnos (se o segmento da vez não for uma pasta)
            if( count($segments) > 0 )
            {
                // Does the requested controller exist in the sub-folder?
                
                $possiveisControllers = array(
                    $segments[0],
                    $this->stringToCamelClass($segments[0]),
                    strtolower($this->stringToCamelMethod($segments[0]))
                );
                
                $achouController = false;
                foreach( $possiveisControllers as $arquivo ){
                    if( file_exists(APPPATH.'controllers/'.$this->fetch_directory().$arquivo.'.php') ){
                        $segments[0] = $arquivo;
                        if( isset($segments[1]) ){
                            $segments[1] = $this->stringToCamelMethod($segments[1]);
                        }
                        $achouController = true;
                    }
                }
                
                if( !$achouController ){
                    if( !file_exists(APPPATH.'controllers/'.$this->fetch_directory().$segments[0].'.php') ){
                        if( !empty($this->routes['404_override']) ){
                            $x = explode('/', $this->routes['404_override']);

                            $this->set_directory('');
                            $this->set_class($x[0]);
                            $this->set_method(isset($x[1]) ? $x[1] : 'index');

                            return $x;
                        }else{
                            show_404($this->fetch_directory().$segments[0]);
                        }
                    }
                }
                
            }
            # Se o segmento da vez não existir e o arquivo Index existir
            elseif( is_file(APPPATH.'controllers/'.$this->directory."Index.php") )
            {
                $this->set_class("Index");
                $this->set_method("index");
                $segments[0] = "Index";
                $segments[1] = "index";
            }
            else
            {
                
                debug($this->default_controller);
                
                // Is the method being specified in the route?
                if( strpos($this->default_controller, '/') !== FALSE ){
                    $x = explode('/', $this->default_controller);

                    $this->set_class($x[0]);
                    $this->set_method($x[1]);
                }else{
                    $this->set_class($this->default_controller);
                    $this->set_method('index');
                }

                // Does the default controller exist in the sub-folder?
                if( !file_exists(APPPATH.'controllers/'.$this->fetch_directory().$this->default_controller.'.php') ){
                    $this->directory = '';
                    return array( );
                }
            }

            return $segments;
        }


        // If we've gotten this far it means that the URI does not correlate to a valid
        // controller class.  We will now see if there is an override
        if( !empty($this->routes['404_override']) ){
            $x = explode('/', $this->routes['404_override']);

            $this->set_class($x[0]);
            $this->set_method(isset($x[1]) ? $x[1] : 'index');

            return $x;
        }


        // Nothing else to do at this point but show a 404
        show_404($segments[0]);
    }

    function set_directory( $dir ){
        // Allow forward slash, but don't allow periods.
        $this->directory = str_replace('.', '', $dir).'/';
    }

    private function stringToCamelMethod( $string = "" )
    {
        return preg_replace_callback("(-.{1})", create_function('$a', 'return str_replace("-","",strtoupper($a[0]));'), $string);
    }

    private function stringToCamelClass( $string = "" )
    {
        return ucfirst($this->stringToCamelMethod($string));
    }

}

/* End of file MY_Router.php */
/* Location: ./application/core/MY_Router.php */