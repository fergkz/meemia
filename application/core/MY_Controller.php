<?php

class MY_Controller extends CI_Controller
{
    protected $modo;
    
    private $cacheFilepath = "";
    private $cacheFilename = "";
    private $cacheTime = 0;
    
    public function session()
    {
        $this->load->library('SessaoLib');
        
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return null;
        }elseif( !array_key_exists(1, $args) ){
            return SessaoLib::get($args[0]);
        }else{
            return SessaoLib::set($args[0], $args[1]);
        }
    }
    
    public function view( $file, $render = array() )
    {
        include_once( APPPATH . "libraries/View.php" );
        $View = new View($file);
        $conteudo = $View->render($render);
        $conteudo = str_replace("\n", " ", $conteudo);
        for( $i = 0; $i < 8; $i++ ){
            $conteudo = str_replace("  ", " ", $conteudo);
        }
        die($conteudo);
        return $View;
    }
    
    public function viewCache( $segundos = null )
    {
        if( $segundos && $segundos > 0 ){
            $this->cacheTime = $segundos;
            
            $this->viewCacheStart();
        }
    }
    
    private function viewCacheStart()
    {
        $this->cacheFilepath = FCPATH."application/cache/paginas";
        
        if( !file_exists($this->cacheFilepath) ){
            mkdir($this->cacheFilepath, 0777, true);
        }
        chmod($this->cacheFilepath, 0777);
        
        $this->cacheFilename = $this->cacheFilepath."/".md5($_SERVER['REQUEST_URI']);
        
        # Procura por todos os caminhos que combinem com o padrão
        $file = glob($this->cacheFilename.'*');
        $filename = @$file[0];
        
        # Tempo em que o arquivo foi gravado
        $filetime = substr($filename, strlen($this->cacheFilename));
        
        # Verifica se o arquivo de cache existe e se a diferença entre o tempo atual e o que o arquivo foi gravado é maior que os segundos informados
        if( file_exists($filename) && (time() - (int)$filetime) < $this->cacheTime ){
            include_once($filename);
            exit;
        }
        
        ob_start();
        return $this;
    }
    
    private function viewCacheEnd(){
        foreach( glob($this->cacheFilename.'*') as $row ){
            unlink($row);
        }
        $content = ob_get_contents();
        ob_end_clean();
        $filename = $this->cacheFilename.time();
        file_put_contents($filename, $content);
        chmod($filename, 0777);
        include_once($filename);
        exit;
    }
    
}