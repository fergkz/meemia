<?php

class MY_Model extends CI_Model
{
    public static $database;
    public static $daoTable;
    public static $daoCols;
    public static $daoPrimary;

    function __construct()
    {
        parent::__construct();
        static::beginTransaction(false);
    }

    public $daoErrorMessage = array();

    public static function beginTransaction( $begin = true )
    {
        $CI = & get_instance();
        if( empty($GLOBALS['connect_model_database']) ){
            $GLOBALS['connect_model_database'] = $CI->load->database('default', TRUE);
        }
        if( $begin ){
            $GLOBALS['connect_model_database']->trans_start();
        }
        $GLOBALS['connect_model_database']->db_debug = 0;
    }

    public static function endTransaction()
    {
        if( $GLOBALS['connect_model_database'] ){
            if( $GLOBALS['connect_model_database']->trans_status() === FALSE ){
                $GLOBALS['connect_model_database']->trans_rollback();
            }else{
                $GLOBALS['connect_model_database']->trans_commit();
            }
            $GLOBALS['connect_model_database']->trans_complete();
            $GLOBALS['connect_model_database']->close();
        }
    }

    public static function rollback()
    {
        $GLOBALS['connect_model_database']->trans_rollback();
    }

    private function clearParse()
    {
        unset($this->_parse);
        unset($this->_pk);
        unset($this->_action);
    }

    public function load( $attributes = null )
    {
        $db = &$GLOBALS['connect_model_database'];
        $db->db_debug = 0;
        foreach( static::$daoPrimary as $attribute => $column ){
            if( empty($this->$attribute) ){
                return false;
            }
            $where[] = $column." = ?";
            $bind[] = $this->$attribute;
        }

        $sql = "SELECT * FROM ".static::$daoTable." WHERE ".implode(" and ", $where);

        $res = $db->query($sql, $bind);

        if( !$res ){
            return false;
        }

        $row = $res->row();

        foreach( static::$daoCols as $attribute => $column ){
            if( !$attributes || in_array($attribute, $attributes) ){
                $this->$attribute = $row->$column;
            }
        }

        $this->clearParse();

        if( method_exists($this, 'triggerAfterLoad') ){
            $this->triggerAfterLoad();
        }

        return $this;
    }

    private function loadQueryParams( $wherePrimary, $bindPrimary )
    {
        $primaryExists = $wherePrimary && count($wherePrimary) > 0 ? true : false;
        if( $this->daoAction !== 'D' ){
            if( $this->daoAction === 'R' ){
                // REPALCE INTO
                if( $primaryExists ){
                    $this->old = clone $this;
                    $this->old->load();
                    unset($this->old->old);
                }else{
                    $class = get_called_class();
                    $this->old = new $class;
                    unset($this->old->old);
                }
                $this->daoAction === 'R';
                foreach( static::$daoCols as $attribute => $column ){
                    $this->$attribute = @$this->$attribute;
                    $bind[] = (!empty($this->$attribute) || is_numeric($this->$attribute) ? $this->$attribute : null);
                    $sqlCols[] = $column;
                    $sqlVals[] = "?";
                }
                $sql = "REPLACE into ".static::$daoTable." (".implode(", ", $sqlCols).") values (".implode(" ,", $sqlVals).")";
            }else if( $primaryExists || $this->daoAction === 'U' ){
                // update
                $this->old = clone $this;
                $this->old->load();
                unset($this->old->old);

                $this->daoAction = "U";
                foreach( static::$daoCols as $attribute => $column ){
                    $bind[] = $this->$attribute;
                    $this->$attribute = $this->$attribute;
                    $sqlCols[] = " {$column} = ? ";
                }
                $sql = "update ".static::$daoTable." set ".implode(", ", $sqlCols)." where ".implode(' and ', $wherePrimary);
                $bind = array_merge($bind, $bindPrimary);
            }else{
                // insert
                $class = get_called_class();
                $this->old = new $class;
                unset($this->old->old);

                $this->daoAction = "I";
                foreach( static::$daoCols as $attribute => $column ){
                    $this->$attribute = @$this->$attribute;
                    $bind[] = (!empty($this->$attribute) || is_numeric($this->$attribute) ? $this->$attribute : null);
                    $sqlCols[] = $column;
                    $sqlVals[] = "?";
                }
                $sql = "insert into ".static::$daoTable." (".implode(", ", $sqlCols).") values (".implode(" ,", $sqlVals).")";
            }
        }else{
            // delete
            $this->old = clone $this;
            $this->old->load();
            unset($this->old->old);

            if( empty($wherePrimary) ){
                return false;
            }
            $sql = "delete from ".static::$daoTable." where ".implode(" and ", $wherePrimary);
            $bind = $bindPrimary;
        }
        return array(
            'sql' => $sql,
            'bind' => $bind
        );
    }

    public function insert()
    {
        return $this->save("I");
    }

    public function update()
    {
        return $this->save("U");
    }

    public function replace()
    {
        return $this->save("R");
    }

    public function delete()
    {
        return $this->save('D');
    }

    public function save( $operacao = null )
    {
        $db = &$GLOBALS['connect_model_database'];
        $db->db_debug = 0;

        $primaryExists = true;
        $primaryAttributes = null;
        if( $operacao !== 'I' ){
            foreach( static::$daoPrimary as $attribute => $column ){
                if( empty($this->$attribute) ){
                    $primaryExists = false;
                }else{
                    $wherePrimary[] = $column." = ?";
                    $bindPrimary[] = $this->$attribute;
                }
                $primaryAttributes[] = $attribute;
            }
        }

        try{
            $this->daoAction = $operacao;
            $this->loadQueryParams(@$wherePrimary, @$bindPrimary);

            /** Begin::Triggers * */
            if( method_exists($this, 'triggerBeforeSave') ){
                $this->triggerBeforeSave();
            }
            /** End::Triggers * */
            $tmp = $this->loadQueryParams(@$wherePrimary, @$bindPrimary);
            $sql = $tmp['sql'];
            $bind = $tmp['bind'];

            $this->daoQuerySql = $sql;
            $this->daoQueryBind = $bind;

            $res = $db->query($sql, $bind);

            if( $res ){
                if( $this->daoAction === 'I' ){
                    $primary = is_array($primaryAttributes) ? $primaryAttributes[0] : $primaryAttributes;
                    if( $primary && !$this->$primary ){
                        $this->$primary = $db->insert_id();
                    }
                }
                /** Begin::Triggers * */
                if( method_exists($this, 'triggerAfterSave') ){
                    $this->triggerAfterSave();
                }
                /** End::Triggers * */
                return $this;
            }else{
                $errorMessage = utf8_encode($db->_error_message());
                $db->trans_rollback();
                $this->daoErrorMessage[] = utf8_decode($errorMessage);
                return false;
            }
        }catch( \Exception $e ){
            $message = $e->getMessage();
            $db->trans_rollback();
            $GLOBALS['connect2_messages']['error'][] = $e->getMessage();
            $this->daoErrorMessage[] = utf8_encode($message);
            return false;
        }
    }

    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array(), $join = null )
    {
        $db = &$GLOBALS['connect_model_database'];
        $where = $bind = array();

        if( $whereColumns ){
            foreach( $whereColumns as $column => $value ){

                if( !in_array((String)$column, static::$daoCols) ){

                    if( !is_array($value) ){
                        if( !is_numeric($column) ){
                            $bind[] = $value;
                        }else{
                            $column = $value;
                        }
                    }else{
                        $arrayVals = array();
                        foreach( $value as $val ){
                            $bind[] = $val;
                        }
                    }

                    $where[] = $column;
                }else{

                    if( !is_array($value) ){
                        $bind[] = $value;
                        $where[] = "{$column} like ?";
                    }else{
                        $arrayVals = array();
                        foreach( $value as $val ){
                            $arrayVals[] = "?";
                            $bind[] = $val;
                        }
                        $vals = implode(",", $arrayVals);
                        $where[] = "{$column} in ({$vals})";
                    }
                }
            }
        }

        if( $loadAttributes && is_array($loadAttributes) && count($loadAttributes) > 0 ){
            foreach( $loadAttributes as $attribute ){
                if( stristr($attribute, ' as ') ){
                    $tmp = explode(' ', trim($attribute));

                    $attribute = array_pop($tmp);
                    array_pop($tmp);

                    $tColumns = trim(implode(' ', $tmp));

                    if( array_key_exists($attribute, static::$daoCols) ){
                        $selectCols[] = "{$tColumns} as ".static::$daoCols[$attribute];
                    }else{
                        $selectCols[] = "{$tColumns} as ".$attribute;
                    }
                }else{
                    $selectCols[] = "dao.".static::$daoCols[$attribute]." as ".static::$daoCols[$attribute];
                }
            }
            $selectCols = implode(", ", $selectCols);
        }else{
            $selectCols = "dao.*";
        }

        $sql = "SELECT {$selectCols} FROM ".static::$daoTable." dao";

        if( $join ){
            $sql .= " {$join} ";
        }

        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }

        $resCont = $db->query("select count(*) as cont_total from ({$sql}) t", $bind);
        $row = $resCont->row();
        $result['cont_total'] = $row->cont_total;

        if( $order && count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }

        if( $rowLimit ){
            $sql .= " limit {$rowStart}, {$rowLimit}";
        }

        $res = $db->query($sql, $bind)->result_array();

        $result['sql'] = $sql;
        $result['bind'] = $bind;

        if( $res ){
            $dados = null;
            foreach( $res as $row ){
                $classname = get_called_class();
                $Obj = new $classname;

                foreach( $row as $column => $value ){
                    if( !$column || !in_array($column, static::$daoCols) ){
                        $Obj->$column = $value;
                    }else{
                        $tmp = array_keys(static::$daoCols, $column);
                        $key = $tmp[0];
                        $Obj->$key = $value;
                    }
                }

                $Obj->clearParse();

                if( method_exists($Obj, 'triggerAfterLoad') ){
                    $Obj->triggerAfterLoad();
                }

                $dados[] = $Obj;
            }

            $result['rows'] = $dados;
        }else{
            $result['rows'] = array();
        }
        return $result;
    }

    public function raise( $message )
    {
        throw new Exception($message);
        return false;
    }

    static public function loadFromArray( $data )
    {
        $class = get_called_class();
        $Obj = new $class;

        foreach( static::$daoCols as $attribute => $column ){
            $Obj->$attribute = $data[$attribute];
        }

        if( method_exists($Obj, 'triggerAfterLoad') ){
            $Obj->triggerAfterLoad();
        }

        return $Obj;
    }

}