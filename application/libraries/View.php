<?php

include_once( __DIR__ . "/Twig.class.php" );

class View
{
    private $Twig;
    private $Template = false;

    public function __construct( $filename, $options = array() )
    {
        $loader = new Twig_Loader_Filesystem(array(
            APPPATH . "views"
        ));
        $this->Twig = new Twig( $loader, $options );
        $this->Twig->declareFunction("base_url");
        $this->Template = $this->Twig->loadTemplate( $filename );
        
        return $this;
    }

    public function setTemplate( $filename ){
        $this->Template = $this->Twig->loadTemplate( $filename );
        return $this;
    }
    
    public function declareFunction( $functionName ){
        $this->Twig->declareFunction($functionName);
        return $this;
    }

    public function display( $render = array() ){
        if( $this->Template ){
            $this->Template->display($render);
            return $this;
        }else{
            die("Nenhum arquivo encontrado");
        }
    }
    
    public function render( array $render = array() ){
        if( $this->Template ){
            return $this->Template->render($render);
        }else{
            die("Nenhum arquivo encontrado");
            exit;
        }
    }
}