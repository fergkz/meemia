<?php

class StringLib
{
    public static function findInto( $blockBefore, $blockAfter )
    {
        
    }

    public static function replaceInto( $string, $blockBefore, $blockAfter, $newContent )
    {
        $start = $end = false;
        $new_string = $string_before = $string_after = "";
        for( $i = 0; $i < strlen($string); $i++ ){
            
            if( substr($string, $i, strlen($blockBefore)) == $blockBefore ){
                $start = true;
                $i = $i + strlen($blockBefore);
                $string_before = substr($string, 0, $i);
            }
            
            if( $start ){
                $new_string .= $string[$i];
                
                if( substr($string, $i, strlen($blockAfter)) == $blockAfter ){
                    $string_after = substr($string, $i);
                    return $string_before.$newContent.$string_after;
                }
            }
        }
    }

    public static function replaceTo( $string, $blockBefore, $blockAfter, $newContent )
    {
        $start = $end = false;
        $new_string = $string_before = $string_after = "";
        for( $i = 0; $i < strlen($string); $i++ ){
            
            if( substr($string, $i, strlen($blockBefore)) == $blockBefore ){
                $start = true;
                $string_before = substr($string, 0, $i);
                $i = $i + strlen($blockBefore);
            }
            
            if( $start ){
                $new_string .= $string[$i];
                
                if( substr($string, $i, strlen($blockAfter)) == $blockAfter ){
                    $string_after = substr($string, $i+strlen($blockAfter));
                    return $string_before.$newContent.$string_after;
                }
            }
        }
    }
}