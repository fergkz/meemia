<?php

include_once("Twig/Autoloader.php");

Twig_Autoloader::register();

class Twig extends \Twig_Environment
{   
    public function declareFunction($function)
    {
        $function = new Twig_SimpleFunction($function,
            $function
        );
        $this->addFunction($function);
        return $this;
    }
}