<?php

class SessaoLib
{
    public function __construct()
    {
        if( empty($_SESSION) ){
            session_start();
        }
    }

    public static function set( $key, $value )
    {
        $_SESSION[$key] = $value;
    }

    public static function get( $key )
    {
        return @$_SESSION[$key];
    }

    public static function delete( $key )
    {
        unset($_SESSION[$key]);
    }

}
