<?php

class Login extends MY_Controller
{
    public function index()
    {
        $render = array();
        if( $_POST ){
            redirect("painel");
            $render['erro'] = "Não conectado ao sistema";
        }
        $this->view("publico/login/index.twig", $render);
    }
}