<script src="../../media/libs/jquery/jquery-2.0.0.min.js" type="text/javascript"></script>
<script src="../../media/libs/jquery/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<link href="../../media/libs/jquery/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>

<style>
    *{
        font-size: 12px;
        font-family: sans-serif;
        background-color: #000;
        color: #FFF;
    }
    .Page{
        background-color: blue;
        display: block;
    }
    .Page h2{
        display: none;
    }
    #documentTitle{
        margin: 0px 0 0 calc(50% - 450px);
        width: 300px;
        position: absolute;
        display: none;
    }
    #documentMetadata{
        margin: 10px 0 0 calc(50% - 450px - 220px);
        width: 220px;
        position: absolute;
    }
    .ul-menu{
        border-top: 1px dotted #FFF;
        margin: 50px 0 0 calc(50% - 450px - 220px);
        position: absolute;
        width: 220px;
        /*background-color: blue;*/
        padding: 10px 0;
    }
    .ul-menu li{
        height: 17px;
        display: block;
        text-decoration: none;
        /*background-color: red;*/
        text-align: right;
        /*float: right;*/
        width: 100%;
        padding: 0;
        font-size: 12px;
    }
    .ul-menu li a{
        text-decoration: none;
        font-weight: bold;
    }
    .ul-menu li a:hover{
        color: #CCC;
    }
    .ImageContainer{
        text-align: center;
        width: 900px;
        margin: 0px 0 0 calc(50% - 450px);
        position: absolute;
        padding: 0 10px;
    }
    .Notes{
        margin: 0px 0 0 calc(50% + 450px + 20px);
        position: absolute;
        width: 180px;
        padding: 0 10px;
    }
    .Notes .title{
        font-size: 18px;
        height: 18px;
        line-height: 18px;
    }
</style>

<?php

$html = file_get_contents('index.html');

$repl1 = '<p class="Notes"><div>';
$repl2 = '</div></p>';

$html = str_replace($repl1, "<div class='Notes'>", $html);
$html = str_replace($repl2, "</div>", $html);

$divHeader = '<div class="menu"><ul class="ul-menu"></ul></div>';
$html = str_replace('<body>', "<body>{$divHeader}", $html);



//$html = str_replace("Exported at: Mon Sep 01 2014 19:48:34 GMT-0300 ", "</div>", $html);

echo urldecode($html);
?>

<script>
    $().ready(function(){
        $(".Page").hide();
        
        var loc = window.location.href;
        var anchor = loc.split("#")[1];
        if( anchor ){
            $("#"+anchor).first().show();
        }else{
            $(".Page").first().show();
        }
        
        setTimeout(function(){
            window.history.go(0);
        },1000);

        var data = $("#documentMetadata").text().split(" ");
//        console.log(data);
        
        $("#documentMetadata").text("Data de Exportação: "+data[4]+" / "+data[3]+" / "+data[5]);
        
        $(".Page").each(function(){
            var title = $("<p/>").addClass('title');
            title.append( $(this).find('h2').first().html() );
            
            $(".ul-menu").append("<li><a href='#"+$(this).attr('id')+"' class='area'>"+title.html()+"</a></li>");
            
            if( $(this).find(".Notes")[0] ){
                $(this).find(".Notes").prepend(title); 
            }else{
                var Notes = $("<div/>").addClass("Notes").appendTo( $(this) );
                $(Notes).prepend(title); 
            }
//            $(this).find(".ImageContainer")[0].appendTo(nDiv);
//            $(this).find(".Notes")[0].appendTo(nDiv);
//            $(this).append(nDiv);
        });
        
        
        $("area, .area").on('click', function(){
            $(".Page").hide();
            $($(this).attr('href')).first().fadeIn();
        });
    });
</script>